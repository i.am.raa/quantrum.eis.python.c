# README #

Подробное описание доступно в статье: [EIS (Elder's Impuse System) с помощью Python и C](https://quantrum.me/712-eis-elder-s-impuse-system-s-pomoshhyu-python-i-c/).

## Database fields (PostgreSQL) ##

* **symbol**	character varying(6)
* **dt**	date
* **open**	bigint [0]
* **high**	bigint [0]
* **low**	bigint [0]
* **close**	bigint [0]
* **volume**	numeric(20,0) [0]
* **adj**	numeric(20,0) NULL

## Additional ##

Example with database downloader [here](https://bitbucket.org/realer/quantrum.chart.by.stamp.corr).